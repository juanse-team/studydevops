package com.juanse.java.cursodevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CursoDevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CursoDevopsApplication.class, args);
	}

}
